#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "fonctions.h"
#define char_max 50
#define nb_cartes_joueur 10
#define nb_cartes 104
#define sleepTime 2

void afficher_regles()
{
  printf("--------------- REGLES DU JEU ---------------");
  printf("\n 1) On décide du nombre de joueurs (entre 2 et 10) \n");
  printf("\n 2) Pour chaque joueur, on décide de s’il est un robot ou non (o/n strictement, pas y, oui, non, etc), puis on saisi son nom. \n");
  printf("\n 3) Le jeu commence, le plateau s’affiche (avec les cartes et leur nombre de têtes de bœuf chacune) , la main du joueur qui doit jouer aussi. \n");
  printf("\n 4) Le premier joueur choisit sa carte, puis le second, jusqu’à ce que tous les joueurs choisissent une carte à jouer. \n");
  printf("\n 5) Celui ayant choisi la carte la plus faible, joue en premier, puis la deuxième la plus petite, etc... \n");
  printf("\n 6) La carte doit être placée dans la ligne dont la dernière carte est la plus proche (mais plus petite, c'est à dire avec la difference la plus petite mais positive). \n");
  printf("\n 7) Si un joueur chosi une carte dont la carte la plus proche est la 5ème dans sa ligne, alors ce joueur ramassera toutes les cartes de cette ligne et posera sa carte en tant que nouvelle première carte. \n");
  printf("\n 8) Si un joueur choisi une carte si faible qu’elle ne peut pas entrer dans aucune ligne, alors il doit ramasser toutes les cartes d’une ligne de son choix. Il posera sa carte en tant que nouvelle première carte. \n");
  printf("\n 9) Fin de tour, si personne ne possède 66 points, alors un nouveau tour commence, en re-mélangeant les 104 cartes et en re-distribuant les cartes au joueurs et au le plateau, sinon, la partie est finie et le classement est affiché. \n\n\n");

  printf("Veuillez appuyer sur une touche puis sur Enter pour continuer: ");
  getchar();
}

Carte creer_Carte(int num)
{

  Carte carte;
  carte.num = num;
  carte.tdb = 0;
  carte.nom_joueur = malloc(char_max * sizeof(char));
  carte.nom_joueur = "*";
  if (num % 5 == 0)
  {
    if (num % 10 == 0)
    {
      carte.tdb = 3;
    }
    else
    {
      carte.tdb = 2;
    }
  }
  if (num % 11 == 0)
  {
    carte.tdb = carte.tdb + 5;
  }
  else if (num % 11 != 0 && num % 5 != 0)
  {
    carte.tdb = 1;
  }
  return carte;
}

Joueur creer_Joueur(int indice_joueur)
{
  Joueur j;
  j.nom = malloc(char_max * sizeof(char));
  char *tmp = malloc(char_max * sizeof(char));
  do
  {
    printf("Le joueur %d est un bot ? (o/n): ", indice_joueur);
    scanf("%s", tmp);
    printf("\n");
  } while (strcmp(tmp, "o") != 0 && strcmp(tmp, "n") != 0);
  if (strcmp(tmp, "o") == 0)
  {
    printf("Veuillez saisir le nom du bot %d : ", indice_joueur);
    scanf("%s", j.nom);
    printf("\n");
    j.bot = 1;
  }
  else
  {
    printf("Veuillez saisir le nom du joueur %d : ", indice_joueur);
    scanf("%s", j.nom);
    j.bot = 0;
    printf("\n");
  }
  j.tab_Cartes = malloc(nb_cartes_joueur * sizeof(Carte));
  j.nb_points = 0;

  return j;
}

void innit_jeu(Jeu *jeu, int nbJ)
{

  jeu->cartes = malloc(nb_cartes * sizeof(Carte));
  jeu->joueurs = malloc(nbJ * sizeof(Joueur));
  jeu->debut = 0;
  jeu->en_cours = 1;

  for (int m = 0; m < 4; m++)
  {
    jeu->debut_plateau[m] = 0;
  }

  for (int i = 1; i <= nb_cartes; i++)
  {
    jeu->cartes[i - 1] = creer_Carte(i);
  }

  for (int j = 0; j < nbJ; j++)
  {
    jeu->joueurs[j] = creer_Joueur(j + 1);
  }

  jeu->plateau = malloc(4 * sizeof(Carte *));
  for (int k = 0; k < 6; k++)
  {
    jeu->plateau[k] = malloc(6 * sizeof(Carte));
  }
}

void affiche_Cartes(Carte *cartes, int nb)
{
  for (int i = 0; i < nb; i++)
  {
    if (cartes[i].num != 0)
    {
      printf("-----------\n");
      if (cartes[i].num < 10)
      {
        printf("|   %d ; %d |\n", cartes[i].num, cartes[i].tdb);
      }
      else if (cartes[i].num < 100)
      {
        printf("|  %d ; %d |\n", cartes[i].num, cartes[i].tdb);
      }
      else
      {
        printf("| %d ; %d |\n", cartes[i].num, cartes[i].tdb);
      }
    }
  }
  printf("-----------\n");
}

void affiche_Cartes_jouees(Carte *cartes, int nb)
{
  int max = 0;
  for (int k = 0; k < nb; k++)
  {
    if (strlen(cartes[k].nom_joueur) > max)
    {
      max = strlen(cartes[k].nom_joueur);
    }
  }
  for (int i = 0; i < nb; i++)
  {
    if (cartes[i].num != 0)
    {
      printf("---------------------------------\n");
      printf("%s", cartes[i].nom_joueur);
      if (max == strlen(cartes[i].nom_joueur))
      {
        printf("   ");
      }else{
        for (int j = 0; j < max - strlen(cartes[i].nom_joueur) + 3; j++)
        {
          printf(" ");
        }
      }
      printf("| %d ; %d |\n", cartes[i].num, cartes[i].tdb);
    }
  }
  printf("---------------------------------\n");
}

void melange_Cartes(Jeu *jeu)
{
  for (int i = 1; i <= nb_cartes; i++)
  {
    int random = rand() % nb_cartes;
    Carte temp = jeu->cartes[i - 1];
    jeu->cartes[i - 1] = jeu->cartes[random];
    jeu->cartes[random] = temp;
  }
}

void dist_Cartes_Joueur(Jeu *jeu, int nbJ)
{
  for (int i = 0; i < nbJ; i++)
  {
    for (int j = 0; j < nb_cartes_joueur; j++)
    {
      jeu->joueurs[i].tab_Cartes[j] = jeu->cartes[jeu->debut];
      jeu->joueurs[i].tab_Cartes[j].nom_joueur = jeu->joueurs[i].nom;
      (jeu->debut)++;
    }
  }
}

void dist_Cartes_Plateau(Jeu *jeu)
{
  for (int i = 0; i < 4; i++)
  {
    jeu->plateau[i][0] = jeu->cartes[jeu->debut];
    (jeu->debut)++;
  }
}

void aff_Cartes_Plateau(Jeu *jeu)
{
  printf("Plateau: \n");
  printf("-------------------------------------------------------------------------------------------------\n");
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 6; j++)
    {
      if (jeu->plateau[i][j].num != 0)
      {
        if (jeu->plateau[i][j].num < 10)
        {
          printf("|   %d ; %d |", jeu->plateau[i][j].num, jeu->plateau[i][j].tdb);
        }
        else if (jeu->plateau[i][j].num < 100)
        {
          printf("|  %d ; %d |", jeu->plateau[i][j].num, jeu->plateau[i][j].tdb);
        }
        else
        {
          printf("| %d ; %d |", jeu->plateau[i][j].num, jeu->plateau[i][j].tdb);
        }

        if (j != 5)
        {
          printf("        ");
        }
      }
    }
    printf("\n");
  }
  printf("-------------------------------------------------------------------------------------------------\n");
}

void echanger_Cartes(Carte *a, Carte *b)
{
  Carte tmp;
  tmp = *a;
  *a = *b;
  *b = tmp;
}

void trier_Cartes(Carte *cartes, int len)
{
  int tmp;
  tmp = 1;
  while (tmp)
  {
    tmp = 0;
    for (int k = 0; k < len - 1; k++)
    {
      if (cartes[k].num > cartes[k + 1].num)
      {
        echanger_Cartes(&(cartes[k]), &(cartes[k + 1]));
        tmp = 1;
      }
    }
  }
}

void echanger(int *a, int *b)
{
  int tmp;
  tmp = *a;
  *a = *b;
  *b = tmp;
}

void trier(int tab[], int ind_tab[], int len)
{
  int tmp;
  tmp = 1;
  while (tmp)
  {
    tmp = 0;
    for (int k = 0; k < len - 1; k++)
    {
      if (tab[k] > tab[k + 1])
      {
        echanger(&(tab[k]), &(tab[k + 1]));
        echanger(&(ind_tab[k]), &(ind_tab[k + 1]));
        tmp = 1;
      }
    }
  }
}

int Gagne(Jeu *jeu, int nbJ)
{
  for (int i = 0; i < nbJ; i++)
  {
    if (jeu->joueurs[i].nb_points >= 66)
    {
      return 1;
    }
  }
  return 0;
}

void echanger_joueur(Joueur *j1, Joueur *j2)
{
  Joueur tmp;
  tmp = *j1;
  *j1 = *j2;
  *j2 = tmp;
}

void trier_classement(Jeu *jeu, int nbJ)
{
  int tmp;
  tmp = 1;
  while (tmp)
  {
    tmp = 0;
    for (int k = 0; k < nbJ - 1; k++)
    {
      if (jeu->joueurs[k].nb_points > jeu->joueurs[k + 1].nb_points)
      {
        echanger_joueur(&(jeu->joueurs[k]), &(jeu->joueurs[k + 1]));
        tmp = 1;
      }
    }
  }
}

void reinnit_jeu(Jeu *jeu, int nbJ)
{
  jeu->cartes = realloc(jeu->cartes, nb_cartes * sizeof(Carte));
  jeu->debut = 0;
  jeu->en_cours = 1;

  for (int m = 0; m < 4; m++)
  {
    jeu->debut_plateau[m] = 0;
  }

  for (int i = 1; i <= nb_cartes; i++)
  {
    jeu->cartes[i - 1] = creer_Carte(i);
  }

  for (int j = 0; j < nbJ; j++)
  {
    jeu->joueurs[j].tab_Cartes = realloc(jeu->joueurs[j].tab_Cartes, nb_cartes_joueur * sizeof(Carte));
  }
}

void afficheTitre()
{
  printf("  ______          ______   __    __  ______        _______   _______   ________  __    __  _______  \n");
  printf(" /      \\        /      \\ /  |  /  |/      |      /       \\ /       \\ /        |/  \\  /  |/       \\ \n");
  printf("/$$$$$$  |      /$$$$$$  |$$ |  $$ |$$$$$$/       $$$$$$$  |$$$$$$$  |$$$$$$$$/ $$  \\ $$ |$$$$$$$  |\n");
  printf("$$ \\__$$/       $$ |  $$ |$$ |  $$ |  $$ |        $$ |__$$ |$$ |__$$ |$$ |__    $$$  \\$$ |$$ |  $$ |\n");
  printf("$$      \\       $$ |  $$ |$$ |  $$ |  $$ |        $$    $$/ $$    $$< $$    |   $$$$  $$ |$$ |  $$ |\n");
  printf("$$$$$$$  |      $$ |_ $$ |$$ |  $$ |  $$ |        $$$$$$$/  $$$$$$$  |$$$$$/    $$ $$ $$ |$$ |  $$ |\n");
  printf("$$ \\__$$ |      $$ / \\$$ |$$ \\__$$ | _$$ |_       $$ |      $$ |  $$ |$$ |_____ $$ |$$$$ |$$ |__$$ |\n");
  printf("$$    $$/       $$ $$ $$< $$    $$/ / $$   |      $$ |      $$ |  $$ |$$       |$$ | $$$ |$$    $$/ \n");
  printf(" $$$$$$/         $$$$$$  | $$$$$$/  $$$$$$/       $$/       $$/   $$/ $$$$$$$$/ $$/   $$/ $$$$$$$/  \n");
  printf("                     $$$/                                                                           \n");
  printf("                                                                                                    \n");
  printf("                                                                                                    \n");
}

void free_jeu(Jeu *jeu, int nbJ)
{
  for (int i = 0; i < nbJ; i++)
  {
    for (int j = 0; j < nb_cartes_joueur; j++)
    {
      free(jeu->joueurs[i].tab_Cartes[j].nom_joueur);
    }
    free(jeu->joueurs[i].tab_Cartes);
    free(jeu->joueurs[i].nom);
  }
  free(jeu->joueurs);

  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < nb_cartes_joueur; j++)
    {
      free(jeu->plateau[i][j].nom_joueur);
    }
    free(jeu->plateau[i]);
  }
  free(jeu->plateau);

  for (int k = 0; k < nb_cartes_joueur; k++)
  {
    free(jeu->cartes[k].nom_joueur);
  }
  free(jeu->cartes);
}

void emptyBuffer()
{
  int c;
  while ((c = getchar()) != '\n' && c != EOF)
    ;
}
