#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "fonctions.h"
#define nb_cartes_joueur 10
#define sleepTime 2

int trouver_indice_Carte(int num, Carte *cartes, int nb)
{
    int iPremier = 0;
    int iDernier = nb;
    int iMillieu;
    int indice = -1;
    while (iPremier <= iDernier && indice == -1)
    {
        iMillieu = (iPremier + iDernier) / 2;
        if (cartes[iMillieu].num == num)
        {
            indice = iMillieu;
        }
        else
        {
            if (cartes[iMillieu].num > num)
            {
                iDernier = iMillieu - 1;
            }
            else
            {
                iPremier = iMillieu + 1;
            }
        }
    }
    return indice;
}

int trouver_Joueur(Jeu *jeu, int nbJ, char *nom)
{
    for (int i = 0; i < nbJ; i++)
    {
        if (strcmp(jeu->joueurs[i].nom, nom) == 0)
        {
            return i;
        }
    }
    return 0;
}

Carte demande_Carte_joueur(Joueur *j)
{
    int num, indice, tmp;
    Carte carte;
    trier_Cartes(j->tab_Cartes, nb_cartes_joueur);
    printf("\nCartes de %s \n", j->nom);
    affiche_Cartes(j->tab_Cartes, nb_cartes_joueur);
    printf("Veuillez saisir le numero de la carte que vous souhaitez jouer: ");
    tmp = scanf("%d", &num);
    emptyBuffer();
    indice = trouver_indice_Carte(num, j->tab_Cartes, nb_cartes_joueur);
    while (tmp == 0 || indice == -1)
    {
        printf("Veuillez saisir une carte presente dans votre main: ");
        tmp = scanf("%d", &num);
        indice = trouver_indice_Carte(num, j->tab_Cartes, nb_cartes_joueur);
        emptyBuffer();
    }
    carte = j->tab_Cartes[indice];
    j->tab_Cartes[indice].num = 0;
    j->tab_Cartes[indice].tdb = 0;
    return carte;
}

Carte joue_Carte_bot(Joueur *j)
{
    Carte carte;
    trier_Cartes(j->tab_Cartes, nb_cartes_joueur);
    printf("Cartes de %s \n", j->nom);
    affiche_Cartes(j->tab_Cartes, nb_cartes_joueur);
    int random;
    do
    {
        random = rand() % nb_cartes_joueur;
    } while (j->tab_Cartes[random].num == 0);
    printf("%s est entrain de réfléchir...\n", j->nom);
    sleep(sleepTime);
    carte = j->tab_Cartes[random];
    j->tab_Cartes[random].num = 0;
    j->tab_Cartes[random].tdb = 0;
    return carte;
}

void ramasser_ligne(Jeu *jeu, int iJoueur, char *nom, int ligne)
{
    for (int i = 0; i <= jeu->debut_plateau[ligne]; i++)
    {
        jeu->joueurs[iJoueur].nb_points = jeu->joueurs[iJoueur].nb_points + jeu->plateau[ligne][i].tdb;
    }
    printf("%s a ramassé la ligne %d \n", jeu->joueurs[iJoueur].nom, ligne + 1);
    jeu->debut_plateau[ligne] = 0;
    jeu->plateau[ligne] = malloc(6 * sizeof(Carte));
}

void tour2jeu(Jeu *jeu, int nbJ)
{
    int round = 1;
    Carte *cartes_a_jouer;
    cartes_a_jouer = malloc(nbJ * sizeof(Carte));
    while (round <= nb_cartes_joueur)
    {
        for (int i = 0; i < nbJ; i++)
        {
            system("clear");
            afficheTitre();
            aff_Cartes_Plateau(jeu);
            if (jeu->joueurs[i].bot == 1)
            {
                cartes_a_jouer[i] = joue_Carte_bot(&(jeu->joueurs[i]));
            }
            else
            {
                cartes_a_jouer[i] = demande_Carte_joueur(&(jeu->joueurs[i]));
            }
            system("clear");
            afficheTitre();
        }
        trier_Cartes(cartes_a_jouer, nbJ);
        aff_Cartes_Plateau(jeu);
        printf("Cartes jouées: \n");
        affiche_Cartes_jouees(cartes_a_jouer, nbJ);
        printf("Veuillez appuyer sur une touche puis sur Enter pour continuer: ");
        getchar();
        emptyBuffer();
        system("clear");
        afficheTitre();

        for (int j = 0; j < nbJ; j++)
        {
            round2jeu(cartes_a_jouer[j], jeu, nbJ);
        }
        printf("\n");
        aff_Cartes_Plateau(jeu);
        printf("\n");
        for (int i = 0; i < nbJ; i++)
        {
            printf("%s a %d points \n", jeu->joueurs[i].nom, jeu->joueurs[i].nb_points);
        }
        printf("\nround %d terminé \n", round);
        printf("Veuillez appuyer sur une touche puis sur Enter pour continuer: ");
        getchar();
        emptyBuffer();
        system("clear");
        afficheTitre();
        round++;
    }
    printf("Tour terminé \n");
    free(cartes_a_jouer);
}

void round2jeu(Carte carte, Jeu *jeu, int nbJ)
{
    int diffNum[4];
    int iMin[4] = {0, 1, 2, 3};
    int min = 0;
    int placee = 0;
    int tmp, ligne;
    int tabPoints[4];
    int pMin[4] = {0, 1, 2, 3};

    for (int i = 0; i < 4; i++)
    {
        diffNum[i] = carte.num - jeu->plateau[i][jeu->debut_plateau[i]].num;
    }
    trier(diffNum, iMin, 4);

    while (placee == 0 && min < 4)
    {
        if (diffNum[min] > 0)
        {
            if (jeu->debut_plateau[iMin[min]] + 1 == 5)
            {
                ramasser_ligne(jeu, trouver_Joueur(jeu, nbJ, carte.nom_joueur), carte.nom_joueur, iMin[min]);
                jeu->plateau[iMin[min]][jeu->debut_plateau[iMin[min]]] = carte;
                placee = 1;
            }
            else
            {
                printf("%s place sa carte dans la ligne %d: \n", carte.nom_joueur, iMin[min] + 1);
                jeu->debut_plateau[iMin[min]]++;
                jeu->plateau[iMin[min]][jeu->debut_plateau[iMin[min]]] = carte;
                placee = 1;
            }
        }
        min++;
    }

    if (placee == 0 && jeu->joueurs[trouver_Joueur(jeu, nbJ, carte.nom_joueur)].bot == 0)
    {
        aff_Cartes_Plateau(jeu);

        printf("%s, vous ne pouvez pas placer votre carte.\nVeuillez saisir la ligne que vous voulez ramasser: ", carte.nom_joueur);
        tmp = scanf("%d", &ligne);
        emptyBuffer();

        while ((ligne < 1 || ligne > 4) || tmp == 0)
        {
            printf("Veuillez saisir un nombre entre 1 et 4: ");
            tmp = scanf("%d", &ligne);
            emptyBuffer();
        }
        ligne--;
        printf("\n");
        ramasser_ligne(jeu, trouver_Joueur(jeu, nbJ, carte.nom_joueur), carte.nom_joueur, ligne);
        jeu->plateau[ligne][jeu->debut_plateau[ligne]] = carte;
    }
    else if (placee == 0)
    {
        for (int i = 0; i < 4; i++)
        {
            tabPoints[i] = jeu->plateau[i][jeu->debut_plateau[i]].tdb;
        }
        trier(tabPoints, pMin, 4);

        ramasser_ligne(jeu, trouver_Joueur(jeu, nbJ, carte.nom_joueur), carte.nom_joueur, pMin[0]);
        jeu->plateau[pMin[0]][jeu->debut_plateau[pMin[0]]] = carte;
    }
}

void classement(Jeu *jeu, int nbJ)
{
    trier_classement(jeu, nbJ);
    printf("Voici le classement : \n");
    for (int i = 0; i < nbJ; i++)
    {
        printf("Top %d : %s  ---  %d points \n", i + 1, jeu->joueurs[i].nom, jeu->joueurs[i].nb_points);
    }
}