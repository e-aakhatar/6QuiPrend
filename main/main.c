#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "fonctions.h"
#define sleepTime 2


int main()
{
  srand(time(NULL));
  Jeu jeu;
  system("clear");
  int nbJ, tmp;
  char reponse[1];

  afficheTitre();
  afficher_regles();
  system("clear");
  afficheTitre();

  do
  {
    printf("Le 6 QUI PREND se joue de 2 à 10 joueurs\nCombien de joueurs êtes-vous : ");
    tmp = scanf("%d", &nbJ);
    emptyBuffer();
    printf("\n");
    if (nbJ < 2 || nbJ > 10)
    {
      printf("Nombre de joueurs incorrect\n");
    }
  } while (nbJ < 2 || nbJ > 10 || tmp == 0);
  system("clear");
  afficheTitre();

  innit_jeu(&jeu, nbJ);

  do
  {
    melange_Cartes(&jeu);
    dist_Cartes_Joueur(&jeu, nbJ);
    dist_Cartes_Plateau(&jeu);
    tour2jeu(&jeu, nbJ);
    jeu.debut = 0;
    for (int m = 0; m < 4; m++)
    {
      jeu.debut_plateau[m] = 0;
      jeu.plateau[m] = malloc(6 * sizeof(Carte));
    }
    if (Gagne(&jeu, nbJ) == 1)
    {
      printf("La partie est terminée !\n");
      classement(&jeu, nbJ);

      printf("Voulez vous rejouer ? o/n: ");
      scanf("%s", reponse);
      if (strcmp(reponse, "o") == 0)
      {
        system("clear");
        afficheTitre();
        afficher_regles();
        system("clear");
        afficheTitre();
        do
        {
          printf("Le 6 QUI PREND se joue de 2 à 10 joueurs\nCombien de joueurs êtes-vous : ");
          tmp = scanf("%d", &nbJ);
          emptyBuffer();
          printf("\n");
          if (nbJ < 2 || nbJ > 10 || tmp == 0)
          {
            printf("Nombre de joueurs incorrect\n");
          }
        } while (nbJ < 2 || nbJ > 10 || tmp == 0);
        innit_jeu(&jeu, nbJ);
      }
      else
      {
        jeu.en_cours = 0;
      }
    }
    else
    {
      printf("----------- Redistribution des cartes pour un nouveau tour -----------\n");
      sleep(sleepTime);
      system("clear");
      afficheTitre();
      reinnit_jeu(&jeu, nbJ);
    }
  } while (jeu.en_cours == 1);
  free_jeu(&jeu, nbJ);
  return 0;
}

