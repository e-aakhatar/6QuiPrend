#ifndef __FONCTIONS_H_
#define __FONCTIONS__H

// Structure pour stocker les informations d'une carte
typedef struct
{

	int num;
	int tdb;
	char *nom_joueur;

} Carte;

// Structure pour stocker les informations d'un joueur
typedef struct
{

	char *nom;
	Carte *tab_Cartes;
	int nb_points;
	int bot;

} Joueur;

// Structure pour stocker les informations de la partie
typedef struct
{

	Carte *cartes;
	Carte **plateau;
	Joueur *joueurs;
	int debut;
	int debut_plateau[4];
	int en_cours;

} Jeu;


/* Auteur : Abdel */
/* Date :  20 juin */
/* Résumé : Fonction qu'affiche les régles du jeu */
/* Entrée(s) : */
void afficher_regles();

/* Auteur : Abdel */
/* Date :  20 mai */
/* Résumé : Fonction qui initialise une carte */
/* Entrée(s) : numero de carte */
/* Sortie(s) :  Carte de jeu avec le nombre de tetes de boeuf correct */
Carte creer_Carte(int num);

/* Auteur : Abdel */
/* Date :  20 mai */
/* Résumé : Fonction qui initalise un joueur */
/* Entrée(s) : Indice du joueur */
/* Sortie(s) :  Joueur de la partie */
Joueur creer_Joueur(int indice_joueur);

/*Auteur : Abdel */
/* Date :  20 mai */
/* Résumé : Procédure qui initalise le jeu en initialisant tous les joueurs 1 par 1
et en initialisant le packet de 104 cartes*/
/* Entrée(s) : Tableau de jeu et nombre de joueurs dans la partie */
void innit_jeu(Jeu *jeu, int nbJ);

/*Auteur : Abdel */
/* Date :  20 mai */
/* Résumé : Procédure qui permet l'affichage d'un tableau de cartes
IMPORTANT: ce tableau doit être de 1D*/
/* Entrée(s) : Tableau de cartes 1D et nombre de cartes dans le tableau */
void affiche_Cartes(Carte *cartes, int nb);

/*Auteur : Abdel */
/* Date :  20 juin */
/* Résumé : Procédure qui permet l'affichage du tableau des cartes qui ont été jouées
difference avec affiche cartes: celle ci affiche le nom qui a joué la carte. */
/* Entrée(s) : Tableau de cartes 1D et nombre de cartes dans le tableau */
void affiche_Cartes_jouees(Carte *cartes, int nb);

/*Auteur : Antonin */
/* Date :  24 mai */
/* Résumé : Procédure qui mélange le jeu de cartes de manière aléatoire*/
/* Entrée(s) : Tableau de jeu afin de pouvoir maanier les cartes du jeu*/

void melange_Cartes(Jeu *jeu);

/*Auteur : Abdel */
/* Date :  24 mai */
/* Résumé : Procédure qui permet de distribuer 10 cartes à l'ensemble
des joueurs de la partie */
/* Entrée(s) : Tableau de jeu et le nombre de joueur de la partie */

void dist_Cartes_Joueur(Jeu *jeu, int nbJ);

/*Auteur : Abdel */
/* Date :  24 mai */
/* Résumé : Procédure qui distribue les 4 cartes sur la première colonne du tableau*/
/* Entrée(s) : Tableau de jeu afin d'avoir accès au plateau */
void dist_Cartes_Plateau(Jeu *jeu);

/*Auteur : Abdel */
/* Date :  26 mai */
/* Résumé : Procédure qui permet l'affichage des cartes du plateau de jeu */
/* Entrée(s) : Tableau de jeu afin d'avoir accès au plateau */
void aff_Cartes_Plateau(Jeu *jeu);

/*Auteur : Ewen */
/* Date :  27 mai */
/* Résumé : Procédure qui echange deux cases d'un tableau de carte
utilisée pour trier les cartes des joueurs et les cartes jouée à chaque round*/
/* Entrée(s) : 2 pointeurs Carte */
void echanger_Cartes(Carte *a, Carte *b);

/*Auteur : Abdel */
/* Date :  27 mai */
/* Résumé : Procedure qui trie un tableau de carte */
/* Entrée(s) : Tableau de cartes et le nombre de carte du tableau */
void trier_Cartes(Carte *cartes, int len); /*Faite Ewen*/

/*Auteur : Ewen */
/* Date :  27 mai */
/* Résumé : Procédure qui permet l'echange de deux cases d'un tableau d'entier
pour le tri à bulle */
/* Entrée(s) : Deux pointeurs d'entiers*/
void echanger(int *a, int *b);

/*Auteur : Abdel */
/* Date :  28 mai */
/* Résumé : Procédure permettant de trier les différences entre les cartes jouées
 pour savoir où placer la carte du joueur*/
/* Entrée(s) : 2 tableaux d'entier, taille du tableau */
void trier(int tab[], int ind_tab[], int len);

/*Auteur : Abdel */
/* Date :  28 mai */
/* Résumé : Fonction qui demande au joueur la carte qu'il veut jouer dans sa main */
/* Entrée(s) : Tableau de joueur afin d'avoir accès aux cartes du joueur */
/* Sortie(s) : La carte jouée par le joueur*/
Carte demande_Carte_joueur(Joueur *j);

/*Auteur : Abdel */
/* Date :  05 juin */
/* Résumé : Fonction qui simule le bot et choisi une carte à jouer */
/* Entrée(s) : Tableau de joueur afin d'avoir accès aux cartes du joueur */
/* Sortie(s) : La carte jouée par le joueur*/
Carte joue_Carte_bot(Joueur *j);

/*Auteur : Abdel */
/* Date :  28 mai */
/* Résumé : Procédure qui permet le déroulement d'un tour de jeu, chaque tour de
jeu est composé de 10 rounds, sachant qu'un round correspond au fait que tous les
joueurs aient chosi leur carte a jouer et l'ont posé. */
/* Entrée(s) : Tableau de jeu et le nombre de joueurs de la partie */
void tour2jeu(Jeu *jeu, int nbJ);

/*Auteur : Abdel */
/* Date :  28 mai */
/* Résumé : Fonction qui permet de trouver l'indice de la carte que le joueur
a décidé de jouer.  */
/* Entrée(s) : Numero de la carte que le joueur souhaite joué, la main du joueur,
et le nombre de carte dans sa main.*/
/* Sortie(s) : Retourne l'indice de la carte, où l'indice correspond a l'emplacement
de la carte choisi dans la main du joueur qui est représenté comme un tableau.*/
int trouver_indice_Carte(int num, Carte *cartes, int nb);

/*Auteurs : Abdel */
/* Date :  28 mai */
/* Résumé : Procédure qui permet le déroulement d'un round de jeu. */
/* Entrée(s) : L'ensemble des cartes jouées par les joueurs, le tableau de jeu,
le nombre de joueurs*/
void round2jeu(Carte carte, Jeu *jeu, int nbJ);

/*Auteurs : Abdel */
/* Date :  28 mai */
/* Résumé : Fonction qui permet de remonter à toutes les informations d'un joueur
à la carte qu'il a posé. Cela est utile lorqu'il pose une carte la forçant a
ramasser une rangée de carte sur le plateau, cette fonction nous permet donc
de retrouver ses infomations afin de lui enlever des points dans la procédure
"ramasser-ligne" par exemple.*/
/* Entrée(s) : PLateau de jeu, nombre de joueur dans la partie, nom du joueur */
/* Sortie(s) : Indice du joueur qui a joué la carte. Coresspondant à l'indice dans
le tableau jeu->joueur[]*/
int trouver_Joueur(Jeu *jeu, int nbJ, char *nom);

/*Auteurs : Abdel */
/* Date :  28 mai */
/* Résumé : Procédure qui pemet de ramasser une ligne lorsqu'un joueur a posé une
carte trop faible ou lorsqu'il pose la 6ème carte de la ligne. */
/* Entrée(s) : Tableau de jeu, indice du joueur qui ramsasse la ligne, son nom,
l'indice de la ligne dans le tableau de jeu.*/
/* Sortie(s) : */

void ramasser_ligne(Jeu *jeu, int iJoueur, char *nom, int ligne);

/*Auteur : Ewen */
/* Date :  30 mai */
/* Résumé : Fonction qui permet de savoir quand on doit terminer la partie.*/
/* Entrée(s) : Tableau de jeu, nombre de joueurs dans la partie*/
/* Sortie(s) : Booléen sous forme d'entier : Renvoi 1 si la partie doit se finir
à la fin du tour. Renvoi 0 dans tous les autres cas, afin de continuer la partie.*/
int Gagne(Jeu *jeu, int nbJ);

/*Auteur : Ewen */
/* Date :  30 mai */
/* Résumé : Procédure qui permet d'afficher le classement et le nombre de points
de chaque joueur en fin de partie*/
/* Entrée(s) : Tableau de jeu, nombre de joueur dans la partie*/

void classement(Jeu *jeu, int nbJ);

/*Auteurs : Ewen */
/* Date :  30 mai */
/* Résumé : Procédure permettant de trier les joueurs de la partie en fonctions
de leur nombre de points*/
/* Entrée(s) : Tableau de jeu, nombre de joueur dans la partie*/

void trier_classement(Jeu *jeu, int nbJ);

/*Auteurs : Abdel et Ewen */
/* Date :  30 mai */
/* Résumé : Permet d'échanger 2 joueurs afin de trier le tableau de joueur
représentant le classement de la partie.*/
/* Entrée(s) : Tableau de jeu, nombre de joueur dans la partie*/
void echanger_joueur(Joueur *j1, Joueur *j2);

/*Auteurs : Abdel */
/* Date :  31 mai */
/* Résumé : Procédure permettant de vider le buffer afin d'éviter certains
dysfonctionnement dans le terminal.*/
/* Entrée(s) :*/
void emptyBuffer();

/*Auteurs : Abdel */
/* Date :  4 juin */
/* Résumé : Procédure permettant de vider le buffer afin d'éviter certains
dysfonctionnement dans le terminal.*/
/* Entrée(s) : pointeur de Jeu, entier nbJ (nombre de joueurs)*/
void reinnit_jeu(Jeu *jeu, int nbJ);

/*Auteurs : Abdel */
/* Date :  31 mai */
/* Résumé : Fonction permettant de liberer une variable Jeu.
Elle libère tous les tableaux dynamiques que cette variable contient, c'est à dire
les cartes et le nom de chaque joueur, le tableau de joueurs, le packet de 104 cartes, le plateau... */
/* Entrée(s) :*/
void free_jeu(Jeu *jeu, int nbJ);

/*Auteurs : Abdel */
/* Date :  31 mai */
/* Résumé : Procédure permettant d'afficher le titre du jeu en ASCII ART */
/* Entrée(s) : */
void afficheTitre();

#endif