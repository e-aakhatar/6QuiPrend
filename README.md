
-Exécuter le programme :  

    1) cloner le projet avec la clé SSH ou le lien HTTPS 
    
    Dans le Terminal: 
    2) cd 6QuiPrend/main
    3) make
    4) ./exe

-Jouer au 6 qui prend : 

    1) On décide du nombre de joueurs (entre 2 et 10)
    2) Pour chaque joueur, on décide de s’il est un robot ou non (o/n strictement, pas y, oui, non, etc), puis on saisi son nom
    3) Le jeu commence, le plateau s’affiche (avec les cartes et leur nombre de têtes de bœuf chacune) , la main du joueur qui doit jouer aussi.
    4) Le premier joueur choisit sa carte, puis le second, jusqu’à ce que tous les joueurs choisissent une carte à jouer.
    5) Celui ayant choisi la carte la plus faible, joue en premier, puis la deuxième la plus petite, etc...
    6) La carte doit être placée dans la ligne dont la dernière carte est la plus proche (mais plus petite, c'est à dire avec la difference la plus petite mais positive)
    7) Si un joueur chosi une carte dont la carte la plus proche est la 5ème dans sa ligne, alors ce joueur ramassera toutes les cartes de cette ligne et posera sa carte en tant que nouvelle première carte.  
    8) Si un joueur choisi une carte si faible qu’elle ne peut pas entrer dans aucune ligne, alors il doit ramasser toutes les cartes d’une ligne de son choix. Il posera sa carte en tant que nouvelle première carte. 
    9) Fin de tour, si personne ne possède 66 points, alors un nouveau tour commence, en re-mélangeant les 104 cartes et en re-distribuant les cartes au joueurs et au le plateau, sinon, la partie est finie et le classement est affiché. 

